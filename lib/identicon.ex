defmodule Identicon do

  @doc """
  Generate a Identicon.Image struct

  ## Examples

      iex> Identicon.main("Adam")
      %Identicon.Image{color: {126, 253, 114},
       grid: [[126, 253, 114, 253, 126], [28, 139, 255, 139, 28],
        [242, 147, 124, 147, 242], 'f#_#f', [45, 13, 186, 13, 45]],
       hex: [126, 253, 114, 28, 139, 255, 242, 147, 124, 102, 35, 95, 45, 13,
        186, 193]}
  """
  def main(input) do
    input
    |> hash_input
    |> Identicon.Image.pick_color
    |> Identicon.Image.build_grid
    |> Identicon.Image.filter_odd_squares
    |> Identicon.Image.build_pixel_map
    |> Identicon.Image.draw_image
    |> save_image(input)
  end

  defp hash_input(input) do
    hex = :crypto.hash(:md5, input)
    |> :binary.bin_to_list

    %Identicon.Image{hex: hex}
  end

  defp save_image(image, filename) do
    File.write("#{filename}.png", image)
  end
end
